// IMPORT: COMPONENTS
import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights';


export default function Home(){

	// DATA TO BE DISPLAYED IN HOME PAGE BANNER
	const data = {
		title: "Happy Mart!",
		subtitle: "",
		content: `“A smile is happiness you'll find right under your nose.”
		― Tom Wilson`
,
		destination: "/products",
		label: "Check our products."
	}

	// PASS DATA TO BANNER PROP AND DISPLAY ALONG WITH HIGHLIGHTS
	return(
		<>
			<Banner bannerProp={data}/>
			<Highlights/>
		</>
	)
}
// IMPORT: BOOTSTRAP ELEMENTS
import {Col, Row, Card} from "react-bootstrap"

// IMPORT: CSS
import './styles/Highlights.css';

// HIGHLIGHTS FUNCTION MAIN --------------------------------------------------------------
export default function Highlights(){
    return(
        <Row className="my-3">
            <Col xs={12} md={4} className="px-4 py-2">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title className="text-center text-sm-start">
                            <h2>Experience a Shopping Spree of a Lifetime</h2>
                        </Card.Title>
                        <Card.Text className="text-justify">
                                                    Join Happy Mart to find everything you need at the best prices. Doing your Happy Mart online shopping at the Philippines’ best marketplace cannot get any easier. Happy Mart is a social marketplace where you can enjoy instant and personalized updates from your friends and favorite community members. If you spot great products or deals while you’re doing your Happy Mart online shopping, Happy Mart enables you to share these deals with your friends via a simple tap.


                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4} className="px-4 py-2">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title className="text-center text-sm-start">
                            <h2>Enjoy Special Deals, Sales, Promos, and Discounts</h2>
                        </Card.Title>
                        <Card.Text className="text-justify">
                                                  Doing your Happy Mart online shopping is not only easy and safe, but it’s also tons of fun. Enjoy big sales like the 9.9 Sale, 11.11 Sale, and 12.12 Sale to score the biggest and best discounts and special prices on your favorite products. Treat yourself during payday with Happy Mart’s Payday Sale on the 15th and 30th of every month. With special promotions from shops such as Happy Mart’s free shipping vouchers, deals, and flash sales, to weekly offers - you’re sure to become a true Happy Martholic. Catch the Happy Mart fever with regular deals on your favorite categories only on Happy Mart Philippines! Download the app on your mobile phone now. If you’re new to the Happy Mart fam, you get the chance to enjoy Happy Mart New User vouchers too! Happy Mart New User vouchers are the perfect way to welcome you into your favorite shopping destination. 


                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4} className="px-4 py-2">
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title className="text-center text-sm-start">
                            <h2>Free shipping</h2>
                        </Card.Title>
                        <Card.Text className="text-justify">
                                                     Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur. Mollit tempor laboris commodo anim mollit magna ea reprehenderit fugiat et reprehenderit tempor. Qui ea Lorem dolor in ad nisi anim. Culpa adipisicing enim et officia exercitation adipisicing.


                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}